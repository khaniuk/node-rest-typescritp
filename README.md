# node-rest-typescritp

Node - Express REST API with TypeScript

## Setting dotenv file

Copy `example.env` to `.env`

```
APP_NAME=test
APP_PORT=3000
APP_KEY=w67d8q9322d88273uhsd    //change value key
```

## Run project

Steps to run this project:

1. Run `yarn install` command
2. Setup database settings inside `ormconfig.json` file
3. Run `yarn start` command

## ORM (typeorm)

Migrate

```
typeorm migration:run

typeorm migration:revert
```

Entity

```
typeorm entity:create User
```
