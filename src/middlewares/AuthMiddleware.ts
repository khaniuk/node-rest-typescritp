import { Request, Response, NextFunction } from "express";
const { verify } = require("jsonwebtoken");

export const authMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { authorization } = req.headers;

  if (authorization) {
    const apiKey = process.env.APP_KEY;
    const token = authorization.replace("Bearer ", "");

    const tokenDecode = await verify(token, apiKey);
    req.body.user = tokenDecode;

    return next();
  }
  return res.status(401).json({ error: "Not authorized" });
};
