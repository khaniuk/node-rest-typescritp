"use strict";

import path from "path";
import * as dotenv from "dotenv";

dotenv.config({ path: path.join(__dirname, "../../.env") });
// require("dotenv").config({ path: path.join(__dirname, "../../.env") });

export const Env = {
  apikey: process.env.API_KEY,
};
