import express, { Request, Response, NextFunction } from "express";
import "express-async-errors";
import cors from "cors";
import path from "path";
import * as dotenv from "dotenv";

import { AppDataSource } from "./data-source";
import { router } from "./routers";

dotenv.config({ path: path.join(__dirname, "../.env") });

const app = express();
const serverPort = process.env.APP_PORT || 3000;

AppDataSource.initialize()
  .then(() => {
    app.use(cors());
    app.use(express.json());
    app.use(router);

    app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
      if (err instanceof Error) {
        return res.status(400).send({ error: err.message });
      }

      return res.status(500).send({ message: "Internal error" });
    });

    app.listen(serverPort, () =>
      console.log("Run http://localhost:" + serverPort)
    );
  })
  .catch((error) => console.log(error));
