import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
  ManyToOne,
} from "typeorm";
import { Role } from "./Role";

@Entity({ name: "users", schema: "public" })
// @Entity("users")
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  roleId: number;

  @ManyToOne(() => Role)
  role: Role;

  /* @OneToMany(() => Role)
  @JoinColumn()
  role: Role; */

  @Column()
  fullname: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  status: boolean;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
