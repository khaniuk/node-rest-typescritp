import { RoleRepository } from "../repositories/RoleRespository";

interface RoleInterface {
  description: string;
  status: boolean;
}

class RoleService {
  async getAll() {
    const role = await RoleRepository.find({
      order: { id: "ASC" },
    });

    return role;
  }

  async create({ description, status }: RoleInterface) {
    const roleRepository = RoleRepository;

    const role = roleRepository.create({
      description,
      status,
    });

    await roleRepository.save(role);

    return role;
  }

  async update({ id }, { description, status }: RoleInterface) {
    const roleRepository = RoleRepository;

    const resultRole = await roleRepository.findOneBy({ id });
    if (!resultRole) {
      throw new Error("Role not found");
    }

    return await roleRepository.update(resultRole.id, {
      description,
      status,
    });
  }

  async delete({ id }) {
    const roleRepository = RoleRepository;

    const resultRole = await roleRepository.findOneBy({ id });
    if (!resultRole) {
      throw new Error("Role not found");
    }

    return await roleRepository.delete(id);
  }
}

export { RoleService };
