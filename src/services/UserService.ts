import { UserRepository } from "../repositories/UserRespository";
import { hash } from "bcryptjs";

interface UserInterface {
  roleId: number;
  fullname: string;
  email: string;
  password: string;
  status: boolean;
}

class UserService {
  async getAll() {
    const user = await UserRepository.find({
      order: { id: "ASC" },
    });

    return user;
  }

  async create({ roleId, fullname, email, password, status }: UserInterface) {
    const userRepository = UserRepository;

    if (!email) {
      throw new Error("Email invalid");
    }

    const resultUser = await userRepository.findOne({ where: { email } });

    if (resultUser) {
      throw new Error("User exist");
    }

    const passwordHash = await hash(password, 10);
    const user = userRepository.create({
      roleId,
      fullname,
      email,
      password: passwordHash,
      status,
    });

    await userRepository.save(user);

    return user;
  }

  async update(
    { id },
    { roleId, fullname, email, password, status }: UserInterface
  ) {
    const userRepository = UserRepository;

    if (!email) {
      throw new Error("Email invalid");
    }

    const resultUser = await userRepository.findOneBy({ id });
    if (!resultUser) {
      throw new Error("User not found");
    }

    const passwordHash = await hash(password, 10);
    return await userRepository.update(resultUser.id, {
      roleId,
      fullname,
      email,
      password: passwordHash,
      status,
    });
  }

  async delete({ id }) {
    const userRepository = UserRepository;

    const resultUser = await userRepository.findOneBy({ id });
    if (!resultUser) {
      throw new Error("User not found");
    }

    return await userRepository.delete(id);
  }
}

export { UserService };
