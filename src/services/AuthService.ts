import { compare } from "bcryptjs";
import { sign } from "jsonwebtoken";
import { UserRepository } from "../repositories/UserRespository";
// import { Env } from "../utils";
// console.log(Env.apikey);

/* import path from "path";
import * as dotenv from "dotenv";

dotenv.config({ path: path.join(__dirname, "../../.env") }); */
// console.log(process.env.APP_KEY);

interface IAuthenticateRequest {
  email: string;
  password: string;
}

class AuthService {
  async login({ email, password }: IAuthenticateRequest) {
    const userRepository = UserRepository;
    const apiKey = process.env.APP_KEY;

    const user = await userRepository.findOne({ where: { email } });
    if (!user) {
      throw new Error("User not found");
    }
    const passwordMatch = await compare(password, user.password);

    if (!passwordMatch) {
      throw new Error("Email/Password incorrect");
    }
    const token = sign({ ius: user.id, email: user.email }, apiKey, {
      subject: user.fullname,
      expiresIn: "1d",
    });

    return token;
  }
}

export { AuthService };
