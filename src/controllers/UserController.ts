import { Request, Response } from "express";
import { UserService } from "../services/UserService";

class UserController {
  async getUsers(req: Request, res: Response) {
    const userService = new UserService();

    const user = await userService.getAll();

    return res.status(200).json(user);
  }

  async createUser(req: Request, res: Response) {
    const { roleId, fullname, email, password, status } = req.body;

    const userService = new UserService();

    const user = await userService.create({
      roleId,
      fullname,
      email,
      password,
      status,
    });

    return res.send(user);
  }

  async updateUser(req: Request, res: Response) {
    const id = Number(req.params.id);
    const { roleId, fullname, email, password, status } = req.body;

    const userService = new UserService();

    const user = await userService.update(
      { id },
      {
        roleId,
        fullname,
        email,
        password,
        status,
      }
    );

    return res.send(user);
  }

  async deleteUser(req: Request, res: Response) {
    const id = Number(req.params.id);

    const userService = new UserService();

    const user = await userService.delete({ id });

    return res.send(user);
  }
}

export { UserController };
