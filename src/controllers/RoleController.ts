import { Request, Response } from "express";
import { RoleService } from "../services/RoleService";

class RoleController {
  async getRoles(req: Request, res: Response) {
    const roleService = new RoleService();

    const role = await roleService.getAll();

    return res.status(200).json(role);
  }

  async createRole(req: Request, res: Response) {
    const { description, status } = req.body;

    const roleService = new RoleService();

    const role = await roleService.create({
      description,
      status,
    });

    return res.send(role);
  }

  async updateRole(req: Request, res: Response) {
    const id = Number(req.params.id);
    const { description, status } = req.body;

    const roleService = new RoleService();

    const role = await roleService.update(
      { id },
      {
        description,
        status,
      }
    );

    return res.send(role);
  }

  async deleteRole(req: Request, res: Response) {
    const id = Number(req.params.id);

    const roleService = new RoleService();

    const role = await roleService.delete({ id });

    return res.send(role);
  }
}

export { RoleController };
