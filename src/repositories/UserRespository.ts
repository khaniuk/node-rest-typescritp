import { AppDataSource } from "../data-source";
import { User } from "../entity/User";

const UserRepository = AppDataSource.getRepository(User).extend({
  findByName(fullname: string) {
    return this.createQueryBuilder("users")
      .where("user.fullname = :fullname", { fullname })
      .getMany();
  },
});

export { UserRepository };
