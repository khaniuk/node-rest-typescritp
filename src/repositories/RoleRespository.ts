import { AppDataSource } from "../data-source";
import { Role } from "../entity/Role";

const RoleRepository = AppDataSource.getRepository(Role);

export { RoleRepository };
