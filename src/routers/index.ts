import { Router } from "express";
import { authMiddleware } from "../middlewares/AuthMiddleware";
import { RoleController } from "../controllers/RoleController";
import { AuthController } from "../controllers/AuthController";
import { UserController } from "../controllers/UserController";

const router = Router();

const authController = new AuthController();
const roleController = new RoleController();
const userController = new UserController();

router.get("/roles", authMiddleware, roleController.getRoles);
router.post("/role", authMiddleware, roleController.createRole);
router.put("/role/:id", authMiddleware, roleController.updateRole);
router.delete("/role/:id", authMiddleware, roleController.deleteRole);

router.post("/login", authController.authUser);

router.get("/users", authMiddleware, userController.getUsers);
router.post("/user", authMiddleware, userController.createUser);
router.put("/user/:id", authMiddleware, userController.updateUser);
router.delete("/user/:id", authMiddleware, userController.deleteUser);
export { router };
